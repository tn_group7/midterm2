/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.midterm2;

/**
 *
 * @author HP
 */
public class Seller {
    protected String name;
    protected String color;
    protected int num;
    
    public Seller(String name){
        this.name = name;
    }
    
    public String sell(String color){
        this.num = 0;
        return this.color = color;
    }
    
    public int sell(int num){
        this.color = null;
        return this.num = num;
    }
    
    public String sell(String color, int num){
        this.color = null;
        this.num = num;
        return null;
    }
    
    
    
    @Override
    public String toString(){
      if(this.color!=null){
          if(this.num>0){
              return this.name + ": Here you have " + this.num + " " + this.color + " candies";
          }
          else{
              return this.name + ": Here your " + this.color + " candies";
          }
      }
      else{
          return this.name + ": Here your " + this.num + " candies";
      }
    }
}
