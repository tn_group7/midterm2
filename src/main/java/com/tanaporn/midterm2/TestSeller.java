/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.midterm2;

/**
 *
 * @author HP
 */
public class TestSeller {
    public static void main(String[] args){
        Seller seller =  new Seller("James");
    
    Customer customer1 = new Customer("Nut");
    seller.sell("red");
    System.out.println(seller);
    System.out.println(customer1);
    System.out.println("");
    
    Customer customer2 = new Customer("Nat");
    seller.sell("green", 5);
    System.out.println(seller);
    System.out.println(customer2);
    System.out.println("");
    
    Customer customer3 = new Customer("Noon");
    seller.sell(4);
    System.out.println(seller);
    System.out.println(customer3);
    System.out.println("");
    
    Customer customer4 = new Customer("Nan");
    seller.sell("blue", 3);
    System.out.println(seller);
    System.out.println(customer4);
    System.out.println("");
    
    Customer customer5 = new Customer("Nuch");
    seller.sell("purple");
    System.out.println(seller);
    System.out.println(customer5);
    System.out.println("");
    } 
}
